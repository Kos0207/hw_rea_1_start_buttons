import './App.css';
import Modal from './components/Modal'

function App() {
  return (
    <div className="App">
      
      
      <Modal header='Do you want to delete this file?' backgroundColor='primary' buttonText='Open first modal' text="Once you delete this file, it won\'t be possible to undo this action. Are you sure you want to delete it?" />
      <Modal header='do you want to add the selected product to the order?' backgroundColor='secondary' buttonText='Open second modal' text="The selected product will be added to the cart with the ordered products"/>
    </div>
  );
}

export default App;
