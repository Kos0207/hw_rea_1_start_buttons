import React, { Component } from "react";
import Button from "../Button";
import './index.scss'

class Modal extends Component {
  constructor(props) {
    super(props);
    this.openModal=this.openModal.bind(this)
    this.state = {
      show: false,
    };
  }
  openModal = () => {
    this.setState({ show: !this.state.show });
  };
  render() {
    const { header, buttonText, backgroundColor, text } = this.props;

    return (
      <>
        <Button buttonText={buttonText} backgroundColor={backgroundColor} onClick={this.openModal}/>
        {this.state.show && (
          <div className="modal show">
            <div className="modal__overlay" onClick={this.openModal}></div>
            <div className={"modal__content"}>
              <div className={"modal__content-top"}>
                <h3 className={"modal__content-header"}>
                 {header}
                </h3>
                <div className={"modal__close-btn close-btn"} onClick={this.openModal}> &times;</div>
              </div>
              <p className={"modal__content-text"}>
               {text}
              </p>
              <div className={"modal__content-bottom"}>
                <button className={"modal__btn"}>Ok</button>
                <button className={"modal__btn"}>Cancel</button>
              </div>
            </div>
          </div>
        )}
      </>
    );
  }
}
export default Modal;
