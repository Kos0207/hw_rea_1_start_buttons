import React, { Component } from 'react'
import './index.scss'
class Button extends Component {
  render() {
    const {buttonText, backgroundColor, onClick} = this.props;
    
    return (
      <button type='button' className={`btn ${backgroundColor}`} onClick={onClick}> {buttonText}</button>
    )
  }
}
export default Button